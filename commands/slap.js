const randomessage = ["Painful, isn't it?", "That was loud.", "You must have said something bad.", "Oh dear, oh dear, what have you done?"];

exports.run = (client, message) => {
  if (message.mentions.users.first() === null)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  if (message.mentions.users.first().bot)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  message.channel.send({embed: {
    title: "",
    color: 16711680,
    description: `${message.mentions.users.first()} You have been slapped by ${message.author}.  \n ${randomessage.random()}`
  }});
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "10"
};

exports.help = {
  name: "slap",
  category: "Fun",
  description: "Lets you to slap a user. \nSidenote: You cannot slap the bot. Really...seriously.",
  usage: "slap [mention]"
};
