const randomessage = ["Love is in the air.", "Chuu! :hearts:", "Don't ever do that again", "Oh my, someone is showing some love here."];

exports.run = (client, message) => {
  if (message.mentions.users.first() === null)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  if (message.mentions.users.first().bot)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  message.channel.send({embed: {
    title: "",
    color: 16726981,
    description: `${message.mentions.users.first()} You have been kissed by ${message.author}. \n ${randomessage.random()}`
  }});
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "10"
};

exports.help = {
  name: "kiss",
  category: "Fun",
  description: "Lets you to kiss a user. \nSidenote: You cannot kiss the bot. Really...seriously.",
  usage: "kiss [mention]"
};
