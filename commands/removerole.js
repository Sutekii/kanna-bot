const Embed = require('discord.js').RichEmbed;
exports.run = async (client, message, args) => {
  const settings = message.settings = client.getGuildSettings(message.guild);
  const role = message.guild.roles.find(role => role.name === args.slice(1).join(' '));
  const member = message.guild.member(message.mentions.users.first());
  const modlog = message.guild.channels.find(x => x.name === settings.modLogChannel);
  if (message.mentions.users.size < 1) return message.reply('You must mention someone to give them role.');
  if (!role) return message.reply('This role doesn\'t exist.');
  if (!modlog) return message.reply('I cannot find a mod-log channel.');
  if (!member.roles.some(r => r === role)) return message.reply('User doesn\'t have this role.');
  const embed = new Embed()
  .setColor(0x00AE86)
  .setTimestamp()
  .setDescription(`**Action:** Remove role\n**Target:** ${member.user.tag}\n**Moderator:** ${message.author.tag}\n**Role:** ${role.name}`);
  await member.removeRole(role);
  return client.channels.get(modlog.id).send({ embed });
};
exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "Administrator"
};

exports.help = {
  name: "removerole",
  category: "Moderation",
  description: "Removes role for user",
  usage: "removerole [user] [role name]"
};
