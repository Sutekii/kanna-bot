const randomessage = ["Hello? Are you still here?", "Looks like someone needs your attention.", "Are you even paying attention?", "Knock, knock, are you awake?"];

exports.run = (client, message) => {
  if (message.mentions.users.first() === null)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  if (message.mentions.users.first().bot)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  message.channel.send({embed: {
    title: "",
    color: 16777011,
    description: `${message.mentions.users.first()} You have been poked by ${message.author}. \n ${randomessage.random()}`
  }});
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "10"
};

exports.help = {
  name: "poke",
  category: "Fun",
  description: "Lets you to poke a user. \nSidenote: You cannot poke the bot. Really...seriously.",
  usage: "poke [mention]"
};
