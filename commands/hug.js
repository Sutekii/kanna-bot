const randomessage = ["Looks like someone wants to spread some love here.", "Good...showing care for others is good.", "Maybe he/she is giving away free hugs?", "The person is giving you a good hug."];

exports.run = (client, message) => {
  if (message.mentions.users.first() === null)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  if (message.mentions.users.first().bot)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  message.channel.send({embed: {
    title: "",
    color: 16726981,
    description: `${message.mentions.users.first()} You have been hugged by ${message.author}. \n ${randomessage.random()}`
  }});
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "10"
};

exports.help = {
  name: "hug",
  category: "Fun",
  description: "Lets you hug a user. \nSidenote: You cannot hug the bot. Really...seriously.",
  usage: "hug [mention]",
};
