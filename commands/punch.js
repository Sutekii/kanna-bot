const randomessage = ["What have you done to deserved this?", "Was it painful?", "Are u fighting with each other? NO FIGHTING PLS."];

exports.run = (client, message) => {
  if (message.mentions.users.first() === null)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  if (message.mentions.users.first().bot)
    return message.channel.send("`Error! you did not specify a valid target please try again`");

  message.channel.send({embed: {
    title: "",
    color: 16711680,
    description: `${message.mentions.users.first()} You have been punched by ${message.author}. \n ${randomessage.random()}`
  }});
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: "10"
};

exports.help = {
  name: "punch",
  category: "Fun",
  description: "Lets you to punch a user. \nSidenote: You cannot punch the bot. Really...seriously.",
  usage: "punch [mention]"
};
