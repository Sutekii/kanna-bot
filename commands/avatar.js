exports.run = (client, message, args) => {
  message.channel.send(message.author.avatarURL);
}

exports.conf = {
  enabled: true,
  guildOnly: false,
  aliases: [],
  permLevel: "User"
};

exports.help = {
  name: "avatar",
  category: "Miscellaneous",
  description: "Gives your own avatar back as a URL",
  usage: "avatar"
};
