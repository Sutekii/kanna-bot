const Embed = require('discord.js').RichEmbed;
exports.run = (client, message, args) => {
  if (args.length < 1) return message.reply("You must supply role name.");
  const settings = message.settings = client.getGuildSettings(message.guild);
  const modlog = message.guild.channels.find(x => x.name === settings.modLogChannel);
  if (!modlog) return message.reply('I cannot find a mod-log channel');
  const roleName = args.join(' ');
  message.guild.createRole({
    name: roleName
  });

  const embed = new Embed()
  .setColor(0x00AE86)
  .setTimestamp()
  .setDescription(`**Action:** Make role\n**Moderator:** ${message.author.tag}\n**Role name:** ${roleName}`);
  return client.channels.get(modlog.id).send({
    embed
  });
};

exports.conf = {
  enabled: true,
  guildOnly: true,
  aliases: [],
  permLevel: 'Administrator'
};

exports.help = {
  name: 'mkrole',
  category: 'Moderation',
  description: 'Command used to create roles.',
  usage: 'mkrole [role name]'
}
