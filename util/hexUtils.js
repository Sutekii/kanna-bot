function isHex(h) {
  const a = parseInt(h,16);
  return (a.toString(16) === h.toLowerCase());
}

function stripHexPrefix(h) {
  return h.replace(/^(0x|#)/,"");
}

module.exports = {isHex, stripHexPrefix};